The ParaView Guide (Community Edition)
======================================

Introduction
------------

This repository contains the *LaTeX* files for the **Community Edition** of
**The ParaView Guide** and **ParaView Catalyst User's Guide**.

You can download the PDF for the latest release or buy a printed version of
**The ParaView Guide** [online][ParaView Books]. In addition to the chapters in
the *Community Edition*, the printed versions include 3 extra chapters that
cover using ParaView for CFD analysis, AMR processing, and case-studies.

Editions
---------

The guide will be updated with each significant release of ParaView.
Versions of the guide for specific ParaView releases starting with ParaView 4.3.1
can be found [here][tags].

[ParaView Books]: http://www.paraview.org/paraview-guide/
[tags]: https://gitlab.kitware.com/paraview/paraview-guide-ce/tags

License
-------

**The ParaView Guide (Community Edition)** and
**ParaView Catalyst User's Guide** are released under [CC BY 4.0][].

The printed editions of **The ParaView Guide** are released under [CC BY-ND 4.0][].
[CC BY 4.0]: http://creativecommons.org/licenses/by/4.0/
[CC BY-ND 4.0]: http://creativecommons.org/licenses/by-nd/4.0/

Build Instructions
------------------

This is **CMake** based project and can be built similar to ParaView. We use **LaTeX**
for typesetting the guides. Thus, unless you are familiar with setting up LaTeX on your system,
you may want to simply download the pregenerated PDF from the [ParaView homepage][ParaView Books].

1.  Clone the repository

        $ git clone https://gitlab.kitware.com/paraview/paraview-guide-ce.git ParaViewGuide
    The main repository will be configured as your `origin` remote.

2.  Run CMake. This step requires you have LaTeX (and necessary modules) installed
    on your system. You can turn **ENABLE_PARAVIEW_GUIDE** (default: ON) and/or
    **ENABLE_CATALYST_USERS_GUIDE** (default: OFF) ON or OFF based on your preference.
    On Unix-based systems, the steps are:

        $ mkdir ParaViewGuildBuild
        $ cmake <PATH>/ParaViewGuide
        $ make

3.  On successful build, the **ParaView/ParaViewUsersGuide.pdf** and/or
    **ParaViewCatalyst/ParaViewCatalystUsersGuide.pdf** will be generated based
    on the options you chose in Step 2.

Note that Step 2 can take a long time, especially *rendering* the images.
To speed things, you can edit [ParaViewUsersGuide.tex][] file to change
the documentclass mode to `[draft]`.

    \documentclass[draft]{InsightSoftwareGuide}
This will generate the the draft PDF without any images.
[ParaViewUsersGuide.tex]: ParaView/ParaViewUsersGuide.tex

Reporting Bugs
---------------

1.  If you have a edits or updates, please see [CONTRIBUTING.md][] document.

2.  For build issues and other inquiries, please join the
    [ParaView Mailing List][].

2.  You can also report issues or errata on the [issue tracker][].

[issue tracker]: https://gitlab.kitware.com/paraview/paraview-guide-ce/issues
[ParaView Mailing List]: http://www.paraview.org/mailing-lists/

Contributing
------------

See [CONTRIBUTING.md][] for instructions to contribute.

[CONTRIBUTING.md]: CONTRIBUTING.md
